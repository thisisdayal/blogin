from django.contrib.auth import get_user_model
from django.urls import reverse
from django.test import TestCase

from .models import Post


class BlogTests(TestCase):
    @classmethod
    def setUpTestData(cls) -> None:
        cls.user = get_user_model().objects.create_user(
            username="tester",
            email="tester@email.com",
            password="secretpass123",
        )
        cls.post = Post.objects.create(
            title="This is a test",
            author=cls.user,
            body="Nice body",
        )

    def test_post_model(self):
        self.assertEqual(self.post.title, "This is a test")
        self.assertEqual(self.post.author.username, "tester")
        self.assertEqual(self.post.author.email, "tester@email.com")
        self.assertEqual(self.post.body, "Nice body")
        self.assertEqual(str(self.post), "This is a test")
        self.assertEqual(self.post.get_absolute_url(), "/post/1/")

    def test_url_exists_at_correct_location_listview(self):
        response = self.client.get("/")
        self.assertEqual(response.status_code, 200)

    def test_url_exists_at_correct_location_detailview(self):
        response = self.client.get("/post/1/")
        self.assertEqual(response.status_code, 200)

    def test_post_listview(self):
        response = self.client.get(reverse("home"))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "pages/home.html")
        self.assertContains(response, "Nice body")

    def test_post_detailview(self):
        response = self.client.get(reverse("post_detail", kwargs={"pk": self.post.pk}))
        no_response = self.client.get("/post/-1/")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(no_response.status_code, 404)
        self.assertContains(response, "This is a test")
        self.assertContains(response, "Nice body")
        self.assertTemplateUsed(response, "pages/detail.html")

    def test_post_createview(self):
        response = self.client.post(
            reverse("post_new"),
            {
                "title": "New post",
                "body": "New body",
                "author": self.user.id,
            },
        )
        self.assertEqual(response.status_code, 302)
        self.assertEqual(Post.objects.last().title, "New post")
        self.assertEqual(Post.objects.last().body, "New body")
        self.assertEqual(Post.objects.last().author.id, self.user.id)

    def test_post_updateview(self):
        response = self.client.post(
            reverse("post_edit", kwargs={"pk": self.post.pk}),
            {
                "title": "Edited title",
                "body": "Edited body",
            },
        )
        self.assertEqual(response.status_code, 302)
        self.assertEqual(Post.objects.last().title, "Edited title")
        self.assertEqual(Post.objects.last().body, "Edited body")
        self.assertEqual(Post.objects.last().author.id, self.user.id)

    def test_post_deleteview(self):
        response = self.client.post(
            reverse("post_delete", kwargs={"pk": self.post.pk}),
        )
        self.assertEqual(response.status_code, 302)

from django.urls import reverse_lazy
from django.views.generic import (
    ListView,
    DetailView,
    CreateView,
    UpdateView,
    DeleteView,
)

from .models import Post


class BlogListView(ListView):
    model = Post
    template_name = "pages/home.html"


class BlogDetailView(DetailView):
    model = Post
    template_name = "pages/detail.html"


class BlogCreateView(CreateView):
    model = Post
    template_name = "pages/post_create.html"
    fields = ["title", "author", "body"]


class BlogUpdateView(UpdateView):
    model = Post
    template_name = "pages/post_edit.html"
    fields = ["title", "body"]


class BlogDeleteView(DeleteView):
    model = Post
    template_name = "pages/post_delete.html"
    success_url = reverse_lazy("home")

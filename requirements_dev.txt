asgiref==3.5.2
Django==4.1.2
environs==9.5.0
factory-boy==3.2.1
Faker==15.1.1
gunicorn==20.1.0
marshmallow==3.17.1
mypy==0.971
mypy-extensions==0.4.3
packaging==21.3
pyparsing==3.0.9
python-dateutil==2.8.2
python-dotenv==0.20.0
six==1.16.0
sqlparse==0.4.3
tomli==2.0.1
typing_extensions==4.4.0
